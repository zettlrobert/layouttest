function init () {

  const d = document;

  // function startTimer() {
  //
  //   let i = 5;
  //   let countdownTimer = setInterval(function() {
  //     console.log(i);
  //     i = i - 1;
  //
  //     if(i<=0) {
  //       clearInterval(countdownTimer);
  //       setTimeout(function(){
  //         console.log("Script Running!");
  //       }, 1000);
  //     }
  //   }, 1000);
  // }
  // startTimer();






  //Screen-2
  let toggleScreen2 =  d.querySelector(".toggleScreen-2");
  let screen2 = d.querySelector(".screen-2");
  let backFromScreen2 = d.querySelector(".backFromScreen2");

  toggleScreen2.addEventListener('click', function() {
    console.log("clicked, " + toggleScreen2);
    console.log(screen2);
    screen2.style.top = '0';
  }),

  backFromScreen2.addEventListener('click', function() {
    console.log("clicked, " + backFromScreen2);
    screen2.style.top = "-100vh";
  })


  //Screen-3
  let toggleScreen3 = d.querySelector(".toggleScreen-3");
  let screen3 = d.querySelector(".screen-3");
  let backFromScreen3 = d.querySelector(".backFromScreen3")

  toggleScreen3.addEventListener('click', function() {
    screen3.style.left = '0';
  })

  backFromScreen3.addEventListener('click', function() {
    screen3.style.left = '-100vw';
  })


  //Screen-4
  let toggleScreen4 = d.querySelector('.toggleScreen-4');
  let screen4 = d.querySelector(".screen-4");
  let backFromScreen4 = d.querySelector('.backFromScreen4');

  toggleScreen4.addEventListener('click', function() {
    screen4.style.left = '0';
  })

  backFromScreen4.addEventListener('click', function() {
    screen4.style.left = '100vw';
  })

}
window.onload = init;
